# gitea_proof

[Verifying my OpenPGP key: openpgp4fpr:2974C1175BF81E46BC4898306D21A9D9F47CC1C8]

This is an OpenPGP proof that connects my OpenPGP key to this Gitea account. For details check out https://keyoxide.org/guides/openpgp-proofs
